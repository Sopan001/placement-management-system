import { Route, Switch } from 'react-router-dom';
import AddActivity from './pages/AddActivity/AddActivity';
import CampusActivity from './pages/CampusActivity/CampusActivity';
import DetailActivity from './pages/DetailActivity/DetailActivity';
import EditActivity from './pages/EditActivity/EditActivity';
import Login from './pages/Login/Login';
import PageNotFound from './pages/PageNotFound/PageNotFound';
import StudentDtails from './pages/StudentDetails/StudentDetails';


function App() {

  return (
    <>
      <Switch>
        <Route path="/" exact component={Login}/>
        <Route path="/campusactivity" component={CampusActivity}/>
        <Route path="/addActivity" component={AddActivity}/>
        <Route path="/editactivity/:companyId" component={EditActivity}/>
        <Route path="/detailactivity/:companyId" component={DetailActivity}/>
        <Route path="/studentDetails/:studentId" component={StudentDtails}/>
        <Route component={PageNotFound}/>
      </Switch>
    </>
  );
}

export default App;
