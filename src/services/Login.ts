import axios from "axios";

interface IForm{
    username:string,
    password:string
} 

const ValidateUser = async (formData:IForm) =>{

    const {data:response} = await axios.post(`/user/login`,formData);

    if(response.data)
    {
        return response.data;
    }
    return null;

}

export default ValidateUser;