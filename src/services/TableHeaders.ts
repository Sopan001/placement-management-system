import { HeadCell } from "../components/Table/Table.types";

export const ActivityheadCells: readonly HeadCell[] = [
    {
      id: 'company',
      numeric: false,
      disablePadding: true,
      label: 'Company',
    },
    {
      id: 'domain',
      numeric: true,
      disablePadding: false,
      label: 'Domain',
    },
    {
      id: 'no_of_students',
      numeric: true,
      disablePadding: false,
      label: 'Number Of Students',
    },
    {
        id: 'action',
        numeric: true,
        disablePadding: false,
        label: 'Action',
      },
  ];



export const StudentDetailsheadCells: readonly HeadCell[] = [
    {
      id: 'student',
      numeric: false,
      disablePadding: true,
      label: 'Student',
    },
    {
      id: 'year',
      numeric: true,
      disablePadding: false,
      label: 'Year',
    },
    {
      id: 'avgPercentage',
      numeric: true,
      disablePadding: false,
      label: 'Average Percentage',
    },
    {
        id: 'cv',
        numeric: true,
        disablePadding: false,
        label: 'CV',
      },
    {
        id: 'action',
        numeric: true,
        disablePadding: false,
        label: 'Action',
      },
  ];