import axios from 'axios';
import FileDownload from "js-file-download";


export const getAllStudents = async(token:string) => {
    const{data:response} = await axios.get("/student",{ headers : {
        "Authorization":token
        }
    })
    const {data,error} = response;  
    if(!error){

        return data;
    }
    return null;
} 

export const getStudentByID = async(token:string,id:string) => {
    const { data:response } = await axios.get(`/student/${id}`,{
        headers : {
            "Authorization":token
            }
    })
    const {data,error} = response; 
    if(!error){

        return data;
    }
    return null;
} 

export const updateStudent = async(token:string,formData:any) => {
    const { data:response } = await axios.put("/student",formData,{
        headers : {
            "Authorization":token
            }
    })

    const { data } = response;
    return data[0];
}

export const uploadCV = async(token:string,id:number,File:any) => {
    const formData = new FormData();
    formData.append("File",File[0]);
    const {data} = await axios.post(`/student/${id}/uploadCv`,formData ,{
        headers : {
            "Authorization":token,
            'Content-Type': 'multipart/form-data'
            }
    })
    return data[0];
}

export const downloadCV = async(token:string,id:number) => {
    
    await fetch(`/student/${id}/downloadCv`,{ headers : {
        "Authorization":token,
        }
    }).then((response) => response.blob()
        .then((blob:any) => { 
            FileDownload(blob, 'resume.pdf');
        })
    ).catch(error => { 
        console.log(error);
    });
}