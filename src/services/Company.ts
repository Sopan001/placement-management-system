import axios from "axios"


export const getCompanyData = async (token:string) => {
    const {data:response} = await axios.get("/company",{ headers : {
        "Authorization":token
        }
    })
    const{data,error} = response;
    
    if(!error){
        return data.data;
    }
    return null;
}


export const getCompanyByID = async (token:string,id:string) => {
    const {data:response} = await axios.get(`/company/${id}`,{ headers : {
        "Authorization":token
        }
    })
    const{data,error} = response;
    if(!error){
        return data;
    }
    return null;
}



interface IFormData {
        name:string,
        domain:string,
        noofstudent:number,
        list:[],
        date:string
}


export const createCompany  = async (token:any,formData:IFormData) => {
    const {data:response} = await axios.post(`/company`,formData,{ headers : {
        "Authorization":token
        }
    })

    const{data,error} = response;
    if(!error){
        return data;
    }

    return null;
}

export const updateCompany = async (token:string,id:string,formData:any) => {
    const {data:response} = await axios.put(`/company/${id}`,formData,{ headers : {
        "Authorization":token
        }
    })
    const{data,error} = response;
    if(!error){
        return data;
    }

    return null;
}