import { Button } from "@mui/material";
import Header from "../../components/Header/Header";
import EnhancedTable from "../../components/Table/Table";
import styles from "./CampusActivity.module.scss";
import AddIcon from '@mui/icons-material/Add';
import { Redirect,useHistory } from "react-router-dom";
import { ActivityheadCells } from "../../services/TableHeaders";
import { useState,useEffect } from "react";
import { getCompanyData } from "../../services/Company";
import Loader from "../../components/loader/Loader";
import AlertBox from "../../components/Alert/Alert";



function CampusActivity(){
    const history = useHistory();
    const [tableData, setTableData] = useState([]);
    const[error,setError] = useState(false);
    const [loader,setLoader] = useState(false);

    const token = localStorage.getItem("token")

    useEffect(() => {
        const getData = async () => {
            if(token){
                setLoader(true);
                const data = await getCompanyData(token);
                if(data){
                    setTableData(data);
                }
                else{
                    setError(true);
                }
                setLoader(false);
            }
        }
        getData();
    }, [token])



    if(!token){
        return <Redirect to="/"/>
    }

    return(
        <>
        { loader ? <Loader/> 
            :
            <div className={styles.container}>
                <Header/>

                <div className={styles.row}>
                        <h2>Todays Campus Activities</h2>

                        <Button variant="contained" className={styles.btn} 
                                onClick={() => history.push("/addActivity")}>
                            <AddIcon/>
                            Add New
                        </Button>
                </div>

                <main className={styles.main}>
                    <EnhancedTable 
                        tableHeader={ActivityheadCells} 
                        rows={tableData} 
                        editAction="editActivity" 
                        detailAction="detailactivity"  
                        firstKey="name" 
                        dataKeys={["domain", "noofstudent"]}
                        title="Companys"
                    />
                </main>

                <AlertBox open={error} type="error" message="Oops!Some Error Occured" updateFun={setError}/>

            </div>
        }
        </>
    )
}
export default CampusActivity;