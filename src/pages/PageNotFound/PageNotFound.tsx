import styles from "./PageNotFound.module.scss";
import pic_404 from "../../images/page_not_found.svg";
import Header from "../../components/Header/Header";

const PageNotFound = () => {
    return(
        <div className={styles.container}>
            <Header/>
            <img src={pic_404} alt="page not found"/>
        </div>

    )
}


export default PageNotFound;