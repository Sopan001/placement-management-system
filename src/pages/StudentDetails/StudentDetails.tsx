import Header from "../../components/Header/Header"
import styles from "./StudentDetails.module.scss";
import EditIcon from '@mui/icons-material/Edit';
import StudentDetailsForm from "../../components/StudentDetailsForm/StudentDetailsForm";
import { useEffect, useState } from "react";
import { Redirect, useParams } from "react-router-dom";
import { getStudentByID } from "../../services/Student";
import Loader from "../../components/loader/Loader";


interface IParam {
    studentId:string
}


const StudentDtails = () => {

    const token = localStorage.getItem("token")

    const {studentId} = useParams<IParam>();

    const[edit,setEdit] = useState(false);

    const[loader,setLoader] = useState(false);

    const[formData,setFormData] = useState({
        id: "",
        name: "",
        year: "",
        avg: ""
    });

    useEffect(() => {

        const getData = async () => {
            setLoader(true);
            if(token){
                const data = await getStudentByID(token,studentId);
                if(data){
                    setFormData(data);
                }
            }
            setLoader(false)
        }

        getData();

    }, [token,studentId])


    if(!token){
        return <Redirect to="/"/>
    }
    
    return(

        <>
        {
         loader ? <Loader/> : 
            <div className={styles.container}>
                <Header/>

                <div className={styles.sectionHeader}>
                    <h2>Student Details</h2>
                    <button className={styles.btn} onClick={() => setEdit(!edit)}>
                        <EditIcon/>
                    </button>
                </div>
                {
                    formData.id &&
                    <StudentDetailsForm data={formData} edit={edit}/>
                }
    
            </div>
        }
        </>
    )
}

export default StudentDtails;