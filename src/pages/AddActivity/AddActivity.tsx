import Header from "../../components/Header/Header";
import styles from "./AddActivity.module.scss";
import EnhancedTable from "../../components/Table/Table";
import ActivityForm from "../../components/ActivityForm/ActivityForm";
import { StudentDetailsheadCells } from "../../services/TableHeaders";
import { useState } from "react";
import { Redirect,useHistory } from "react-router-dom";
import { createCompany } from "../../services/Company";
import { downloadCV } from "../../services/Student";
import AlertBox from "../../components/Alert/Alert";



const AddActivity = () => {

    const token = localStorage.getItem("token");
    const history = useHistory();
    const [alert,setAlert] = useState(false);

    const[tableData,setTableData] = useState([]);
    
    const formData = {
        company:"",
        date:null,
        domain:"",
        no_of_vacancies:""
    }


    const handleFormSubmit = async(formData:any) => {
        const response = await createCompany(token,formData);
        if(response)
        {   
            history.push(`/detailactivity/${response.id}`);
            return "Company Added SuccessFully";
        }
        return null;
    }

    const DownloadCv = (id:number) =>{
        if(token){
         downloadCV(token,id)
        }
    }

    const DeleteAction = (id:number) => {
        const filterData = tableData.filter((data:any) => data.id !== id)
        setTableData(filterData);
        setAlert(true);
    }

    if(!token){
        return <Redirect to="/"/>
    }


    return(
            <div className={styles.container}>
                <Header/>

                <ActivityForm 
                    formData={formData} 
                    tableData={tableData} 
                    setTableData={setTableData} 
                    handleFormSubmit={handleFormSubmit}
                    title="Add New Activity"
                />

                <div className={styles.tableContainer}>
                    <EnhancedTable 
                        rows={tableData} 
                        tableHeader={StudentDetailsheadCells} 
                        deleteAction={DeleteAction}  
                        downloadAction={DownloadCv} 
                        firstKey="name" 
                        dataKeys={["year", "avg"]}
                        title="Students"
                    />
                </div>

                <AlertBox open={alert} type="warning" message="Student Removed" updateFun={setAlert}/>      
            </div>
    )
}
export default AddActivity;