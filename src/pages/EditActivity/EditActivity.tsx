import Header from "../../components/Header/Header";
import styles from "./EditActivity.module.scss";
import EnhancedTable from "../../components/Table/Table";
import ActivityForm from "../../components/ActivityForm/ActivityForm";
import { StudentDetailsheadCells } from "../../services/TableHeaders";
import { useEffect, useState } from "react";
import { Redirect,useParams } from "react-router-dom";
import { getCompanyByID,updateCompany } from "../../services/Company";
import { downloadCV } from "../../services/Student";
import AlertBox from "../../components/Alert/Alert";
import Loader from "../../components/loader/Loader";


interface IParams {
    companyId:string
}

const EditActivity = () => {

    const token = localStorage.getItem("token");
    const[tableData,setTableData] = useState([]);
    const { companyId } = useParams<IParams>()
    const [alert,setAlert] = useState(false);
    const[loader,setLoader] = useState(false);
    const[formData,setFormData] = useState({
        name:"",
        date:"",
        domain:"",
        no_of_vacancies:""
    });


    useEffect(() => {
        const getData = async() =>{
            if(token)
            {   
                setLoader(true);
                const data = await getCompanyByID(token,companyId);

                if(data){
                    const{companyDetails,result} = data;
                    if(companyDetails){
                        setFormData({
                            name: companyDetails.name,
                            domain : companyDetails.domain,
                            no_of_vacancies : companyDetails.noofstudent,
                            date : companyDetails.date
                        })
                    }
                    if(result){
                        setTableData(result.data)
                    }
                }
                setLoader(false);
            }

        }

        if(token){
            getData();
        }

    }, [token,companyId])

    if(!token){
        return <Redirect to="/"/>
    }

    const handleFormSubmit = async(formData:any) => {
        const response = await updateCompany(token,companyId,formData);
        if(response){
            return "Company Data Updated Successfully"
        }
        return null
    }

    const DownloadCv = (id:number) =>{
        if(token){
         downloadCV(token,id)
        }
    }

    const DeleteAction = (id:number) => {
        const filterData = tableData.filter((data:any) => data.id !== id)
        setTableData(filterData);
        setAlert(true)
    }

 
    return(
        <>
        {
            loader ? <Loader/> :
            <div className={styles.container}>
                <Header/>

                {
                    formData.name &&
                    <ActivityForm formData={formData} tableData={tableData} setTableData={setTableData} handleFormSubmit={handleFormSubmit} title="Edit Activity"/>
                }
                <div className={styles.tableContainer}>
                    <EnhancedTable 
                        rows={tableData} 
                        tableHeader={StudentDetailsheadCells} 
                        deleteAction={DeleteAction}  
                        downloadAction={DownloadCv} 
                        firstKey="name" 
                        dataKeys={["year", "avg"]}
                        title="Students"
                    />
                </div>

                <AlertBox open={alert} type="success" message="Student Removed" updateFun={setAlert}/>      
            </div>
        }
        </>
    )
}
export default EditActivity;