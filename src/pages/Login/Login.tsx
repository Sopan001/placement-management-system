import styles from "./Login.module.scss";
import Header from "../../components/Header/Header";
import login_img from "../../images/login.png";
import LoginForm from "../../components/LoginForm/LoginForm";


const Login = () => {

    return(

        <section className={styles.container}>
            <Header/>

            <main className={styles.main}>
                <div className={styles.left}>
                    <img src={login_img} alt="card banner"/>       
                </div>
                <div className={styles.right}>
                    <LoginForm/>
                </div>
            </main>
        </section>
                    

    )
}

export default Login;