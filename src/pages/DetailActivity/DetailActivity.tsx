import { useEffect, useState } from "react";
import { Redirect,useParams } from "react-router-dom";
import AlertBox from "../../components/Alert/Alert";
import DetailsHeader from "../../components/DetailsHeader/DetailsHeader";
import Header from "../../components/Header/Header";
import Loader from "../../components/loader/Loader";
import EnhancedTable from "../../components/Table/Table";
import { getCompanyByID } from "../../services/Company";
import { downloadCV } from "../../services/Student";
import { StudentDetailsheadCells } from "../../services/TableHeaders";
import styles from "./DetailActivity.module.scss";
import { IParams } from "./DetailActivity.types";


function DetailActivity(){

    const token = localStorage.getItem("token")

    const { companyId } = useParams<IParams>();
    const [tableData, setTableData] = useState([]);
    const [companyDetails,setCompanyDetails] = useState({
        name:"",
        domain:"",
        noofstudent:""
    });
    const[alert,setAlert] = useState(false);
    const [loader,setLoader] = useState(false);

    useEffect(() => {
        const getData = async () => {

            if(token){
                setLoader(true);
                const response = await getCompanyByID(token,companyId);
                if(response){
                    const {companyDetails,result} = response;
                    const {data} = result;
                    setTableData(data);
                    setCompanyDetails(companyDetails);
                }
                else{
                    setAlert(true)
                }
                setLoader(false);
            }
        }
        if(companyId){
            getData();
        }
    }, [token,companyId])


    const DownloadCv = (id:number) =>{
        if(token){
         downloadCV(token,id)
        }
    }


    if(!token){
        return <Redirect to="/"/>
    }



    return(
        <>
        { loader ? <Loader/> :
        <section className={styles.container}>
        <Header/>

        {/* <div className={styles.sectionHeader}>
            <h2 className={styles.title}>Detail Activity Page</h2>

            <div className={styles.activityDetails}>
            <table>
                <thead>
                    <tr>
                        <th>Company Name</th>
                        <th>Domain</th>
                        <th>No. Of Students</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{companyDetails.name}</td>
                        <td>{companyDetails.domain}</td>
                        <td>{companyDetails.noofstudent}</td>
                    </tr>

                </tbody>
            </table>
            </div>
        </div> */}

        <DetailsHeader {...companyDetails}/>
        <div className={styles.tableContainer}>
            <EnhancedTable 
                rows={tableData} 
                tableHeader={StudentDetailsheadCells} 
                detailAction="studentdetails"  
                downloadAction={DownloadCv} 
                firstKey="name" 
                dataKeys={["year", "avg"]}
                title="Students"
            />
        </div>

        <AlertBox  
            open={alert} 
            type="error" 
            message="Oops!Some Error Occured" 
            updateFun={setAlert}
        />

        </section>
        }
    </>
    )
}
export default DetailActivity;