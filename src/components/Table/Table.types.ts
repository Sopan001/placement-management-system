export interface ActivityData {
    company: string,
    domain:string,
    no_of_students:number,
    action:string
}

export interface StudentData {
    student:string,
    year:string,
    avgPercentage:string,
    cv:string,
    action:string
}

export type Order = 'asc' | 'desc';


export interface HeadCell {
    disablePadding: boolean;
    id: keyof ActivityData | keyof StudentData;
    label: string;
    numeric: boolean;
  }
  
export interface EnhancedTableProps {
    numSelected: number;
    onRequestSort: (event: React.MouseEvent<unknown>, property: keyof ActivityData | keyof StudentData) => void;
    onSelectAllClick: (event: React.ChangeEvent<HTMLInputElement>) => void;
    order: Order;
    orderBy: string;
    rowCount: number;
    tableHeader: any;
}

export interface EnhancedTableToolbarProps {
    numSelected: number;
    title:string;
}