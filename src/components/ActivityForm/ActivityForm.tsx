import MobileDatePicker from "@mui/lab/MobileDatePicker/MobileDatePicker";
import TextField from "@mui/material/TextField";
import React from "react";
import { Controller, useForm } from "react-hook-form";
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import { Button } from "@mui/material";
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from "yup";
import styles from "./ActivityForm.module.scss";
import StudetntList from "../StudentList/StudentList";
import { format } from 'date-fns';
import AlertBox from "../Alert/Alert";
import { IForm,IProps } from "./ActivityForm.types";
import { useState } from "react";


const schema = yup.object().shape({
    name:yup.string().required("*Company Name is required"),
    date:yup.string().nullable().required("*Date is required"),
    domain:yup.string().required("*Domain is required"),
    no_of_vacancies:yup.string().required("*Number of vacancies is required")
});



function ActivityForm({formData,tableData,setTableData,handleFormSubmit,title}:IProps){


    const token = localStorage.getItem("token");

    const [open, setOpen] = useState(false);
    const [avoidIds,setAvoidIDS] = useState([]);
    const [alert,setAlert] = useState(false);
    const [alertMsg,setAlertMsg] = useState({
        type:"error",
        msg:"Oops!Some Error Occured"
    })



    const { register, handleSubmit, control,formState:{ errors } } = useForm<IForm>({
        defaultValues:formData,
        resolver: yupResolver(schema)
    });


    const handleClose = () => setOpen(false);


    //save action
    const onSubmit = async(data:any) => {

        const fDate =  format(new Date(data.date), 'yyyy-MM-dd')
        const ids = tableData.map(({id}:any) => id);
        setAvoidIDS(ids);

        const formData = {
            name:data.name,
            domain:data.domain,
            noofstudent:data.no_of_vacancies,
            list:ids,
            date:fDate
        }

        const response = await handleFormSubmit(formData);

        if(response)
        {   
            setAlert(true);
            setAlertMsg({
                type:"success",
                msg:response
            });
        }
        else{
            setAlert(true);
            setAlertMsg({
                type:"error",
                msg:"Oops!Some error occured"
            });
        }

    }


    const handleAddStudent = () => {
        const ids = tableData.map(({id}:any) => id);
        setAvoidIDS(ids);
        setOpen(true);  
    }   


    return(
        <form className={styles.form} onSubmit={handleSubmit(onSubmit)}>
        <div className={styles.sectionHeader}>
                <h2 className={styles.title}>{title}</h2>

                <Button variant="contained" className={styles.btn} type="submit">
                    Save
                </Button>
        </div>

        <div className={styles.row}>
                <div className={styles.column}>
                    <span>Company</span>
                    <div className={styles.inputBox}>
                        <TextField 
                            {...register("name")} 
                            className={styles.input} 
                            label="Company Name" 
                            variant="outlined" 
                            autoComplete="off"
                        />
                        <p className={styles.error}>{errors.name?.message}</p>
                    </div>
                </div>

                <div className={styles.column}>
                    <span>Date</span>

                    <div className={styles.inputBox}>
                        <LocalizationProvider dateAdapter={AdapterDateFns}>
                        <Controller
                            name={"date"}
                            control={control}
                            render={({ field }:any) => (
                            <MobileDatePicker
                                label="Date"
                                inputFormat="yyyy-MM-dd"
                                value={field.value}
                                onChange={e => field.onChange(e)}
                                renderInput={(params) => <TextField {...params} className={styles.date}/>}
                            />
                            )}/>
                        </LocalizationProvider>
                        <p className={styles.dateError}>{errors.date?.message}</p>
                    </div>
                </div>

                <div className={styles.column}/>
        </div>        

        
        <div className={styles.row}>
                <div className={styles.column}>
                    <span>Domain</span>

                    <div className={styles.inputBox}>
                        <TextField {...register("domain")} 
                            className={styles.input} 
                            label="Domain Name" 
                            variant="outlined" 
                            autoComplete="off"
                        />
                        <p className={styles.error}>{errors.domain?.message}</p>
                    </div>

                </div>
                <div className={styles.column}>
                    <span>Vacancies</span>
                    <div className={styles.inputBox}>
                        <TextField type="number" 
                                {...register("no_of_vacancies")} 
                                className={styles.input} 
                                label="Vacancies" 
                                variant="outlined" 
                                autoComplete="off"/>
                        <p className={styles.error}>{errors.no_of_vacancies?.message}</p>
                    </div>
                </div>
                
                <div className={styles.column2}>
                    <Button variant="contained" className={styles.btn} onClick={handleAddStudent}>Add Student</Button>
                </div>
                
                {
                    token &&
                    <StudetntList open={open} 
                                  handleClose={handleClose} 
                                  tableData = {tableData} 
                                  setTableData = {setTableData} 
                                  avoidIds={avoidIds}
                            />
                }

            </div>      


            <AlertBox open={alert} type={alertMsg.type} message={alertMsg.msg} updateFun={setAlert}/>      

    </form>

    )
}

export default ActivityForm;
