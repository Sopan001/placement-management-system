export interface IForm{
    name:string,
    date:string|null,
    domain:string,
    no_of_vacancies:string
}

export interface IProps{
    formData:any,
    tableData:any,
    setTableData:(tableData:any) => any,
    handleFormSubmit: any,
    title:string;
}