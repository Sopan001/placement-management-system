import { Link } from "react-router-dom";
import styles from "./Header.module.scss";

const Header = () => {
    return(
        <header className={styles.header}>
            <Link to="/campusactivity">
                <h1 className={styles.title}>
                    <span>C</span>ampus
                </h1>
            </Link>
        </header>
    )
}

export default Header;