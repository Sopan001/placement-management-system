import styles from "./Loader.module.scss";

const Loader = () => {
    return(
        <div className={styles.loader}>
            <ul>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
            </ul>
        </div>
    );
}

export default Loader;