import styles from "./Input.module.scss";
import { IProps } from "./Input.types";

const Input = ({type,Icon,name,placeholder,register}:IProps) => {
    return(
        <div className={styles.container}>
            <Icon className={styles.icon}/>
            <input type={type}
                  placeholder={placeholder} 
                  {...register(name)} 
                  autoComplete="off"
            />
        </div>
    )
}

export default Input;