import TextField from "@mui/material/TextField";
import { Button } from "@mui/material";
import { styled } from '@mui/material/styles';
import styles from "./StudentDetailsForm.module.scss";
import CloudUploadIcon from '@mui/icons-material/CloudUpload';
import { useForm, useWatch } from "react-hook-form";
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from "yup";
import {  useState } from "react";
import { downloadCV, updateStudent, uploadCV } from "../../services/Student";
import CloudDownloadIcon from '@mui/icons-material/CloudDownload';
import { IForm, IProp } from "./StudentDetailsForm.types";
import AlertBox from "../Alert/Alert";

const Input = styled('input')({
    display: 'none',
  });
  

const schema = yup.object().shape({
        name: yup.string().required("*Name is required"),
        year: yup.number().required("*Year is required"),
        avg: yup.number().required("*Average score is required"),
        file: yup.mixed()
});



const StudentDetailsForm = ({data,edit}:IProp) =>{

    const token = localStorage.getItem("token");
    const [alert,setAlert] = useState(false);
    const [alertMsg,setAlertMsg] = useState({
        type:"error",
        msg:"Oops!Some error occured"
    })

    const { register, handleSubmit, control,formState:{ errors } } = useForm<IForm>({
        resolver: yupResolver(schema),
        defaultValues: data
    });

    const onsubmit = async(data : IForm) => {
        const {file,...details} = data;

        console.log()
        if(token){
            const updateResponse = await updateStudent(token,details);
            let uploadResponse = 1;
            if(Object.keys(file).length > 0)
            {
                uploadResponse = await uploadCV(token,details.id,file);
            }

            if(updateResponse && uploadResponse){
                setAlert(true);
                setAlertMsg({
                    type:"success",
                    msg:"Data Updated Successfully"
                })
            }
            else if(!uploadResponse){
                setAlert(true);
                setAlertMsg({
                    type:"error",
                    msg:"Please Upload the CV again!"
                })
            }
            else{
                setAlert(true);
                setAlertMsg({
                    type:"error",
                    msg:"Oops!Some error occured"
                })
            }
        }        
    }


    const DownloadCv = (id:number) =>{
        if(token){
         downloadCV(token,id)
        }
    }

    function IsolateReRender({ control } : any) {
        const file = useWatch({
          control,
          name: 'file', 
          defaultValue: ""
        }); 
 
        return <span className={styles.fileName}>{file ? file[0].name : "resume.pdf" }</span>; 
      }
    


    return(
        <form className={styles.form} onSubmit={handleSubmit(onsubmit)}>

            <div className={styles.title}>
                <h2>Details</h2>
            </div>

            <div className={styles.row}>
                <span>Name</span>
                    <TextField disabled={!edit} 
                                label="Name" 
                                variant="outlined" 
                                {...register("name")}
                                autoComplete="off"
                                error={Boolean(errors.name)}
                            />
            </div>

            <div className={styles.row}>
                <span>Year</span>
    
                    <TextField  type="number"
                                disabled={!edit} 
                                label="Year" 
                                variant="outlined" 
                                autoComplete="off"
                                {...register("year")}
                                error={Boolean(errors.year)}
                            />

            </div>

            <div className={styles.row}>
                <span>AVG %</span>

                    <TextField  type="number"
                                disabled={!edit} 
                                label="Avg" 
                                variant="outlined" 
                                autoComplete="off"
                                {...register("avg")}
                                error={Boolean(errors.avg)}
                            />
            </div>

            <div className={styles.row}>
                <span>Resume</span>
                <label htmlFor="cvBtn" className={styles.UploadBtnContainer}>
                {   
                    edit ?
                    <>
                        <Input accept="application/pdf" 
                                id="cvBtn" 
                                type="file" 
                                {...register("file")}
                        />
                        <Button variant="contained" component="span" className={styles.uploadBtn}>
                            <CloudUploadIcon className={styles.icon}/>
                            Upload
                        </Button>
                        <IsolateReRender control={control} />
                    </>
                    :   <Button variant="contained" component="span" className={styles.downloadBtn} onClick={ () => DownloadCv(data.id)}>
                            <CloudDownloadIcon className={styles.icon}/>
                            Download
                        </Button>
                }
                </label>
            </div>

            <div className={styles.btnContainer}>
                {
                    edit &&
                    <Button variant="contained" className={styles.btn} type="submit">
                        Save
                    </Button>
                }
            </div>

            <AlertBox open={alert} type={alertMsg.type} message={alertMsg.msg} updateFun={setAlert}/>
        </form>
    )
}

export default StudentDetailsForm;;