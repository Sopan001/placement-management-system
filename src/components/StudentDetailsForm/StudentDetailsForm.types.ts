export interface IForm{
    id:number;
    name:string;
    year:string;
    avg:string;
    file:object;
}

export interface IProp{
    data:any,
    edit:boolean
}