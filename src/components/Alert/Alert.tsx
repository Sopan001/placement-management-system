import { IProps } from "./Alert.types";
import Snackbar from '@mui/material/Snackbar';
import MuiAlert, { AlertProps } from '@mui/material/Alert';
import React from "react";

const Alert = React.forwardRef<HTMLDivElement, AlertProps>(function Alert(
  props,
  ref,
) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

const AlertBox = ({open,type,message,updateFun}:IProps) => {

    const vertical = 'bottom';
    const horizontal = 'center';

    const handleClose = (event?: React.SyntheticEvent, reason?: string) => {
        if (reason === 'clickaway') {
          return;
        } 
        updateFun(false);
      };


    return(    
    <Snackbar anchorOrigin={{vertical,horizontal}} open={open} autoHideDuration={2000} onClose={handleClose}>
        <Alert severity= {type}  onClose={handleClose}>
                {message}
        </Alert>
    </Snackbar>
    )

}


export default AlertBox;