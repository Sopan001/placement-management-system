import Input from "../Input/Input";
import styles from "./LoginForm.module.scss";
import PersonIcon from '@mui/icons-material/Person';
import LockIcon from '@mui/icons-material/Lock';
import Button from "../Button/Button";
import { Link, useHistory } from "react-router-dom";
import { useForm } from "react-hook-form";
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from "yup";
import { IForm } from "./LoginForm.types";
import Alert from "../Alert/Alert";
import { useState } from "react";
import ValidateUser from "../../services/Login";



const schema = yup.object().shape({
    username: yup.string().required("*Username is required"),
    password: yup.string().required("*Password is required")
});



function LoginForm() {

    
    const history = useHistory();
    const[error,setError] = useState(false);

    const { register, handleSubmit, formState:{ errors } } = useForm<IForm>({
        resolver: yupResolver(schema)
    });

    //Submit Handler
    const onsubmit = async (data : IForm) => {
        
        const result = await ValidateUser(data);

        if(result){
            setError(false);
            localStorage.setItem("token",result);
            history.push("/campusactivity");
        }
        else{

            setError(true);
        }


    }

    return(
        <form className={styles.form} onSubmit={handleSubmit(onsubmit)}>

            <div className={styles.title}>
                <h1>Welcome Back!</h1>
                <span>Login To Continue</span>
            </div>

            <Input name="username" type="text" placeholder="Enter Username" Icon={PersonIcon} register={register}/>
            <p className={styles.errorMsg}>{errors.username?.message}</p>

            <Input name="password" type="password" placeholder="Enter Password" Icon={LockIcon} register={register}/>
            <p className={styles.errorMsg}>{errors.password?.message}</p>

            <Button message="Login"/>

            <Link to="/home" className={styles.forgotPassword}>Forgot Password?</Link>
            

            <Alert open={error} type="error" message="Invalid Username or Password" updateFun={setError}/>
    
        </form>
    )
}

export default LoginForm;