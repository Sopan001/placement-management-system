import Modal from '@mui/material/Modal';
import styles from "./StudentList.module.scss";
import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import FormHelperText from '@mui/material/FormHelperText';
import { useEffect, useState } from 'react';
import Button from '@mui/material/Button';
import { getAllStudents } from '../../services/Student';
import { IProps } from './StudentList.types';


const StudetntList = ({open,handleClose,tableData,setTableData,avoidIds}:IProps) => {

    const token = localStorage.getItem("token");

    const[value,setValue] = useState("");
    const [studentList,setStudentList] = useState([]);
    const [options,setOptions] = useState([]);

    const handleChange = (event: SelectChangeEvent) => {
        setValue(event.target.value);
      };
    
    useEffect(() => {
        const getData = async () => {
            if(token){
                const { data } = await getAllStudents(token);
                setStudentList(data)
            }
        }
        getData();
    }, [token])

    
    useEffect(() => {

        if(studentList){
            if(avoidIds.length > 0)
            {   
                let filteredData = [];
                filteredData = studentList.filter((s:any) => {
                    return !avoidIds.find((e: any) => {
                        return e === s.id;
                    })
                });
                setOptions(filteredData)
            }
            else{
                setOptions(studentList)
            }
        }
        else{
            setOptions([]);
        }

    },[tableData,studentList,avoidIds])


    const handleClick = () => {
        if(value){
            const data = options.filter((student:any) => student.id === value);
            setTableData([...tableData,data[0]]);
        }
        setValue("");
        handleClose();
    }
    
    return(
        <div>
            <Modal open={open} onClose={handleClose} className={styles.modal}>
                <div className={styles.container}>

                    <h2>Add Student</h2>

                    <FormControl className={styles.select}>
                        <InputLabel id="studentName">Student Name</InputLabel>
                        <Select
                            labelId="studentName"
                            value={value}
                            label="Student Name"
                            onChange={handleChange}
                            required
                        >
                        <MenuItem value="">
                            <em>None</em>
                        </MenuItem>
                        
                        {
                            options.map((option:any,index:number) => <MenuItem value={option.id} key={index}>{option.name}</MenuItem>)
                        }


                        </Select>
                        <FormHelperText>Select Student Name</FormHelperText>
                    </FormControl>


                <Button variant="contained" className={styles.btn} onClick={handleClick}>
                    Add
                </Button>
                </div>
            </Modal>
        </div>
    )
}

export default StudetntList;