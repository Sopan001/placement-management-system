export interface IProps{
    open:boolean,
    tableData : any, 
    avoidIds : any,
    handleClose:() => void,
    setTableData: any,
}