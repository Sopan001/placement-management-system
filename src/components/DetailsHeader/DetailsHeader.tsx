import styles from "./DetailsHeader.module.scss";

interface IProps {
    name:string;
    domain:string;
    noofstudent:string;
}

const DetailsHeader = ({name,domain,noofstudent}:IProps) => {
    return(
        <div className={styles.sectionHeader}>
            <h2 className={styles.title}>Detail Activity Page</h2>

            <div className={styles.activityDetails}>
                <table>
                    <thead>
                        <tr>
                            <th>Company Name</th>
                            <th>Domain</th>
                            <th>No. Of Students</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{name}</td>
                            <td>{domain}</td>
                            <td>{noofstudent}</td>
                        </tr>

                    </tbody>
                </table>
            </div>
       </div>
    )
}

export default DetailsHeader;