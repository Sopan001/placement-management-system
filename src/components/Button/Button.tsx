import styles from "./Button.module.scss";
import { IProps } from "./Button.types";

const Button = ({message}:IProps) => {
    return(
        <button className={styles.btn}>{message}</button>
    )
}

export default Button;